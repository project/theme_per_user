# Theme Per User

[![GitLab CI][gitlab-ci-badge]][gitlab-ci]

Theme per User module allow end users to choose the site's theme to their
liking.

[gitlab-ci]: https://git.drupalcode.org/project/drush_shell/-/pipelines?page=1&scope=all&ref=1.0.x
[gitlab-ci-badge]: https://git.drupalcode.org/project/drush_shell/badges/1.0.x/pipeline.svg?key_text=Branch%201.0.x&key_width=90

## Key Features

* Allow end users with permission to select their theme after login.
* Allow administrators to assign permission to choose theme to specific user
  role.

## Installation

Use composer command:

```sh
composer require "drupal/theme_per_user"
```

Then follow normal procedures to install an ordinary contributed Drupal module.

For more instructions, please see:
https://www.drupal.org/docs/extending-drupal/installing-modules


# Configuration
Login as Admin & Go to 'admin/config/people/theme-per-user-settings' and
select which themes to be available to users to change the page appearance.

Login as End User and goto '/user' page
Go to 'Select Theme' tab and select any theme to change the page appearance.


# Maintainers
Koala Yeung - https://www.drupal.org/u/yookoala
Sarath Kumar Bhavanasi - https://www.drupal.org/u/bhavanasisarath
