<?php

namespace Drupal\theme_per_user;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ThemeHandlerInterface;

/**
 * To get user available themes information.
 *
 * @package Drupal\theme_per_user
 */
class ThemeInfoService {

  const TYPE_ADMIN = 1;
  const TYPE_USER = 2;

  /**
   * The theme handler.
   *
   * @var Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * The theme settings.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $settings;

  public function __construct(
    ThemeHandlerInterface $themeHandler,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->themeHandler = $themeHandler;
    $this->settings = $configFactory->get('theme_per_user.settings');
  }

  /**
   * To get user available themes information.
   *
   * @return array
   *   An assoc array of themes information with theme name as key.
   *   Each theme info contains "name", "machine_name", "description" and
   *   "screenshot".
   */
  public function getAllThemesInfo(): array {
    $themes = $this->themeHandler->rebuildThemeData();
    $available_themes = [];

    foreach ($themes as $theme_name => $theme) {
      $available_themes[$theme_name]['name'] = $theme->info['name'];
      $available_themes[$theme_name]['machine_name'] = $theme_name;
      $available_themes[$theme_name]['description'] = $theme->info['name'];
      $available_themes[$theme_name]['screenshot'] = $theme->info['screenshot'];
    }

    return $available_themes;
  }

  /**
   * To get user available theme keys and names.
   *
   * @param int $type
   *   To determine whether admin or user to get the keys.
   *
   * @return array
   *   An assoc array of themes information with theme name as key.
   *   Each theme info contains "name" and "screenshot".
   *
   * @throws InvalidArgumentException
   */
  public function getAvailableThemesInfo(int $type = self::TYPE_USER): array {
    if (!in_array($type, [self::TYPE_ADMIN, self::TYPE_USER])) {
      throw new \InvalidArgumentException('Invalid type');
    }

    $keys = [];

    /** @var \Drupal\Core\Extension\Extension[] */
    $themes = $this->themeHandler->rebuildThemeData();

    if ($type === static::TYPE_ADMIN) {
      foreach ($themes as $theme_name => $theme) {
        // All enabled themes are shown to admin
        // (i.e. let admin decide which to allow end users to use)
        if ($theme->status === 1) {
          $keys[$theme_name]['name'] = $theme->info['name'];
          $keys[$theme_name]['screenshot'] = $theme->info['screenshot'];
        }
      }
    }
    elseif ($type === static::TYPE_USER) {
      $admin_selected_themes = $this->settings->get('admin_selected_themes');
      if (!empty($admin_selected_themes)) {
        $admin_selected_themes = array_filter(array_values($admin_selected_themes));
        foreach ($themes as $theme_name => $theme) {
          // Only themes that are both admin-selected and enabled are shown to
          // end users.
          if (in_array($theme_name, $admin_selected_themes) && $theme->status === 1) {
            $keys[$theme_name]['name'] = $theme->info['name'];
            $keys[$theme_name]['screenshot'] = $theme->info['screenshot'];
          }
        }
      }
    }

    return $keys;
  }

}
