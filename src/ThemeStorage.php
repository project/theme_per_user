<?php

namespace Drupal\theme_per_user;

use Drupal\Core\Database\Connection;

/**
 * Theme storage class to get and set user selected theme info.
 */
class ThemeStorage implements ThemeStorageInterface {

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a Connection object.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function getTheme($user_id) {
    $query = $this->connection->select('theme_per_user');
    $query->fields('theme_per_user', ['theme_name']);
    $query->condition('uid', $user_id, '=');
    $result = $query->execute()->fetchField();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function setTheme($user_id, $theme_name) {
    return $this->connection
      ->insert('theme_per_user')
      ->fields([
        'uid' => $user_id,
        'theme_name' => $theme_name,
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function updateTheme($user_id, $theme_name) {
    return $this->connection
      ->update('theme_per_user')
      ->fields([
        'theme_name' => $theme_name,
      ])
      ->condition('uid', $user_id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTheme($user_id) {
    return $this->connection
      ->delete('theme_per_user')
      ->condition('uid', $user_id)
      ->execute();
  }

}
