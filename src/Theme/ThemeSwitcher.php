<?php

namespace Drupal\theme_per_user\Theme;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\theme_per_user\ThemeInfoService;
use Drupal\theme_per_user\ThemeStorageInterface;

/**
 * Custom Theme switcher class.
 */
class ThemeSwitcher implements ThemeNegotiatorInterface {

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Protected theme variable to store the theme to active.
   *
   * @var string
   */
  protected $theme = NULL;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The theme information service.
   *
   * @var \Drupal\theme_per_user\ThemeInfoService
   */
  protected $themeInfoService;

  /**
   * To get user selected theme information.
   *
   * @var \Drupal\theme_per_user\ThemeStorage
   */
  protected $themeStorage;

  /**
   * Constructs a new ThemeSwitcher.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Account Proxy.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   Admin context.
   * @param \Drupal\theme_per_user\ThemeStorageInterface $theme_storage
   *   Theme Storage.
   * @param \Drupal\theme_per_user\ThemeInfoService $theme_info_service
   *   Theme information service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   Theme Handler.
   */
  public function __construct(
    AccountInterface $current_user,
    AdminContext $admin_context,
    ThemeStorageInterface $theme_storage,
    ThemeInfoService $theme_info_service,
    ThemeHandlerInterface $theme_handler,
  ) {
    $this->currentUser = $current_user;
    $this->adminContext = $admin_context;
    $this->themeStorage = $theme_storage;
    $this->themeInfoService = $theme_info_service;
    $this->themeHandler = $theme_handler;
  }

  /**
   * Whether this theme negotiator should be used to set the theme.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match object.
   *
   * @return bool
   *   TRUE if this negotiator should be used or FALSE to let other negotiators
   *   decide.
   */
  public function applies(RouteMatchInterface $route_match) {
    $user_id = $this->currentUser->id();
    if ($user_id) {
      $this->theme = $this->themeStorage->getTheme($user_id);

      $user_avialable_themes = $this->themeInfoService
        ->getAvailableThemesInfo(ThemeInfoService::TYPE_USER);

      if (!empty($user_avialable_themes) && !in_array($this->theme, array_keys($user_avialable_themes))) {
        $this->theme = $this->themeHandler->getDefault();
      }

      $is_admin = $this->adminContext->isAdminRoute();
      if ($is_admin) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->theme;
  }

}
