<?php

namespace Drupal\theme_per_user;

/**
 * Defines a common interface for book outline storage classes.
 */
interface ThemeStorageInterface {

  /**
   * Gets user selected theme name.
   *
   * @return string
   *   User selected theme name.
   */
  public function getTheme($user_id);

  /**
   * Setting Theme for user.
   *
   * @param string $user_id
   *   UserID.
   * @param string $theme_name
   *   Theme name.
   */
  public function setTheme($user_id, $theme_name);

  /**
   * Updating Theme for user.
   *
   * @param string $user_id
   *   UserID.
   * @param string $theme_name
   *   Theme name.
   */
  public function updateTheme($user_id, $theme_name);

  /**
   * Deleting user entry from table.
   *
   * @param string $user_id
   *   UserID.
   */
  public function deleteTheme($user_id);

}
