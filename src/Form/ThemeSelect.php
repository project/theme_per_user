<?php

namespace Drupal\theme_per_user\Form;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\theme_per_user\ThemeInfoService;
use Drupal\theme_per_user\ThemeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Theme select form for end user.
 */
class ThemeSelect extends FormBase {

  /**
   * To get user selected theme information.
   *
   * @var \Drupal\theme_per_user\ThemeStorage
   */
  protected $themeStorage;

  /**
   * Theme information service.
   *
   * @var \Drupal\theme_per_user\ThemeInfoService
   */
  protected $themeInfoService;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ThemeHandler.
   *
   * @var Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new ThemeSelect.
   *
   * @param \Drupal\theme_per_user\ThemeStorageInterface $theme_storage
   *   Theme Storage.
   * @param \Drupal\theme_per_user\ThemeInfoService $theme_info_service
   *   Theme Information Service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   Account Proxy.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   Theme Handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    ThemeStorageInterface $theme_storage,
    ThemeInfoService $theme_info_service,
    AccountProxy $current_user,
    ThemeHandlerInterface $theme_handler,
    RendererInterface $renderer,
  ) {
    $this->themeStorage = $theme_storage;
    $this->currentUser = $current_user;
    $this->themeHandler = $theme_handler;
    $this->renderer = $renderer;
    $this->themeInfoService = $theme_info_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('theme_per_user.theme_storage'),
      $container->get('theme_per_user.theme_info'),
      $container->get('current_user'),
      $container->get('theme_handler'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'theme-select-per-user';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $user_id = $this->currentUser->id();
    $selected_theme = $this->themeStorage->getTheme($user_id);

    $themes_list = $this->themeInfoService->getAllThemesInfo();

    $available_themes = $this->themeInfoService->getAvailableThemesInfo(ThemeInfoService::TYPE_USER);
    $default_theme = $this->themeHandler->getDefault();

    $header = [
      'theme_name' => $this->t('Theme Name'),
      'screenshot' => $this->t('Screenshot'),
    ];

    if (empty($selected_theme) || (!empty($available_themes) && !in_array($selected_theme, array_keys($available_themes)))) {
      $selected_theme = $default_theme;
    }

    $user_theme = $themes_list[$selected_theme];

    $form['selected_theme_info'] = [
      '#markup' => $this->t('Selected Theme Information:'),
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    $form['selected_theme'] = [
      '#title' => $this->t('Selected Theme Information'),
      '#type' => 'table',
      '#header' => $header,
    ];

    $form['selected_theme'][0]['theme_name'] = [
      '#markup' => $user_theme['name'],
    ];

    $screenshot = [
      '#theme' => 'image',
      '#uri' => $user_theme['screenshot'],
      '#width' => 300,
      '#height' => 200,
    ];
    $form['selected_theme'][0]['screenshot'] = [
      '#markup' => $this->renderer->renderPlain($screenshot),
    ];
    $form['divider'] = [
      '#markup' => '<div class="divider"></div>',
    ];

    $options = [];

    if (!empty($available_themes)) {
      $form['available_themes_info'] = [
        '#markup' => $this->t('Available themes to select:'),
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
      ];
      if ($selected_theme != $default_theme) {
        // Adding default theme to the list.
        $options[$default_theme]['theme_name'] = [
          '#markup' => $this->t('Default'),
        ];
        $default_screenshot = [
          '#theme' => 'image',
          '#uri' => $themes_list[$default_theme]['screenshot'],
          '#width' => 300,
          '#height' => 200,
        ];

        $options[$default_theme]['screenshot'] = $this->renderer->renderPlain($default_screenshot);
      }

      unset($available_themes[$selected_theme]);
      foreach ($available_themes as $key => $theme) {
        $options[$key]['theme_name'] = [
          '#markup' => $theme['name'],
        ];
        $screenshot = [
          '#theme' => 'image',
          '#uri' => $theme['screenshot'],
          '#width' => 300,
          '#height' => 200,
        ];
        $options[$key]['screenshot'] = $this->renderer->renderPlain($screenshot);
      }
    }

    $form['user_available_themes'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#multiple' => FALSE,
      '#empty' => $this->t('No Installed themes available. Please reach out to site administrator to install.'),
      '#default_value' => $selected_theme,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Select Theme'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $theme_name = $form_state->getValue('user_available_themes');

    $user_id = $this->currentUser->id();

    $user_selected_theme = $this->themeStorage->getTheme($user_id);

    if (!empty($theme_name)) {
      if (empty($user_selected_theme)) {
        $this->themeStorage->setTheme($user_id, $theme_name);
      }
      else {
        $this->themeStorage->updateTheme($user_id, $theme_name);
      }
    }
    else {
      $selected_theme = $form_state->getValue('selected_theme');
      $default_theme = $this->themeHandler->getDefault();
      if (!empty($selected_theme) && ($theme_name == $default_theme)) {
        $this->themeStorage->deleteTheme($user_id);
      }
      elseif (!empty($user_selected_theme)) {
        $this->themeStorage->deleteTheme($user_id);
      }
    }
  }

}
