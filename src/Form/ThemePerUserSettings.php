<?php

namespace Drupal\theme_per_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\theme_per_user\ThemeInfoService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings page for admin to select themes for users.
 */
class ThemePerUserSettings extends ConfigFormBase {

  /**
   * Theme information service.
   *
   * @var \Drupal\theme_per_user\ThemeInfoService
   */
  protected ThemeInfoService $themeInfoService;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new ThemePerUserSettings.
   *
   * @param \Drupal\theme_per_user\ThemeInfoService $themeInfoService
   *   The theme information service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    ThemeInfoService $themeInfoService,
    RendererInterface $renderer,
  ) {
    $this->themeInfoService = $themeInfoService;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('theme_per_user.theme_info'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['theme_per_user.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'theme_per_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('theme_per_user.settings');
    $selected_themes = $config->get('admin_selected_themes');

    $available_themes = $this->themeInfoService
      ->getAvailableThemesInfo(ThemeInfoService::TYPE_ADMIN);

    $header = [
      'theme_name' => $this->t('Theme Name'),
      'screenshot' => $this->t('Screenshot'),
    ];
    $options = [];

    if (!empty($available_themes)) {
      foreach ($available_themes as $key => $theme) {
        $options[$key]['theme_name'] = [
          '#markup' => $theme['name'],
        ];
        $screenshot = [
          '#theme' => 'image',
          '#uri' => $theme['screenshot'],
          '#width' => 300,
          '#height' => 200,
        ];
        $options[$key]['screenshot'] = $this->renderer->renderPlain($screenshot);
      }
    }

    $form['admin_available_themes'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('Please install themes for users to change their page appearance.'),
      '#default_value' => !empty($selected_themes) ? $selected_themes : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $selected_themes = $form_state->getValue('admin_available_themes');

    $this->config('theme_per_user.settings')
      ->set('admin_selected_themes', $selected_themes)->save();
  }

}
